# 基于IMX6ULL和STM32MP157的rt-smart移植
## 1. rt-smart介绍

RT-Thread Smart（简称 rt-smart）是基于 RT-Thread 操作系统衍生的新分支，面向带 MMU，中高端应用的芯片，例如 ARM Cortex-A 系列芯片，MIPS 芯片，带 MMU 的 RISC-V 芯片等。rt-smart 在 RT-Thread 操作系统的基础上启用独立、完整的进程方式，同时以混合微内核模式执行。



## 2. 教程介绍

作为个新推出的RTOS，它支持的芯片还不够多。rt-smart非常优秀，我们受rtthread官方邀请，针对IMX6ULL、STM32MP157两款芯片移植了rt-smart，并且录制了课程。希望能为国产RTOS的发展提供一份力量。

本教程支持三款开发板：100ASK_IMX6ULL pro、100ASK_IMX6ULL mini emmc、100ASK_STM32MP157  pro。

* 100ASK_IMX6ULL pro：全功能版

![](IMX6ULL/doc_pic/01_教程说明_资料下载_体验/pic/01_教程说明_资料下载_体验/016_100ask_imx6ll_pro.png)



* 100ASK_IMX6ULL mini emmc

  ![](IMX6ULL/doc_pic/01_教程说明_资料下载_体验/pic/01_教程说明_资料下载_体验/017_100ask_imx6ll_mini_emmc.png)



* 100ASK_STM32MP157  pro：全功能版

  ![](IMX6ULL/doc_pic/01_教程说明_资料下载_体验/pic/01_教程说明_资料下载_体验/018_100ask_stm32mo157_pro.png)



## 3. 资料下载

先去https://gitforwindows.org/下载Windows版本的git工具。

安装、启动Git Bash。

然后执行命令：

```
git clone https://e.coding.net/weidongshan/rt-smart/doc_and_source_for_rt-smart.git
git clone https://e.coding.net/weidongshan/noos/doc_and_source_for_mcu_mpu.git
```



为了方便记忆，上述地址都放在百问网下载中心：http://download.100ask.net/



## 4. 观看视频

* 官网：http://www.100ask.net
* 微信小程序：搜“韦东山”
* rtthread官网视频中心：https://www.rt-thread.org/page/video.html
* B站：
STM32MP157：https://www.bilibili.com/video/BV19A411s7f9/
  IMX6ULL：https://www.bilibili.com/video/BV1ti4y1w7VQ/



## 5. 更新记录

2020.12.23 正式发布，最小系统移植完结
2020.12.24 解决BUG: 可以支持IMX6ULL EMMC启动
2021.01.19 添加Python版本的mkimage，请阅读:

* `IMX6ULL\doc_pic\04_添加一个单板\01_添加一个单板.md`
* `STM32MP157\doc_pic\04_添加一个单板\01_添加一个单板.md`



## 6. 联系方式

* 官网：http://www.100ask.net

* 淘宝：http://100ask.taobao.com

* 微信小程序：搜“韦东山”

* 公众号：
  ![](wechat.jpg)

  