## 指定GIC地址

本节修改好的代码放在GIT仓库里：

```
doc_and_source_for_rt-smart\STM32MP157\source\02_录制视频时编写的源码\A7\02_memmap_stm32mp157\stm32mp157
```

### 1. GIC介绍

GIC名为`Generic Interrupt Controller`，通用的中断控制器。
简单地说，GPIO、UART等各类模块的中断，发给GIC，再由GIC发给某个Processor。
GIC内部结构可以分为2部分：Distributor(分发器)、CPU Interface N(CPU接口)，如下图：

![](pic/05_内存映射/25_gic.png)

① 分发器(Distributor)
	系统中的所有中断源都连接到该单元。可以通过仲裁单元的寄存器来控制各个中断源的属性，例如优先级、状态、安全性、路由信息和使能状态。
分发器把中断输出到“CPU接口单元”，后者决定将哪个中断转发给CPU核。

② CPU接口单元（CPU Interface）
	CPU核通过控制器的CPU接口单元接收中断。CPU接口单元寄存器用于屏蔽，识别和控制转发到CPU核的中断的状态。系统中的每个CPU核心都有一个单独的CPU接口。
	中断在软件中由一个称为中断ID的数字标识。中断ID唯一对应于一个中断源。软件可以使用中断ID来识别中断源并调用相应的处理程序来处理中断。呈现给软件的中断ID由系统设计确定，一般在SOC的数据手册有记录。

如果你想深入理解GIC，可以观看[百问网](http://www.100ask.net)的视频：**单片机RTOS必备/硬件编程**。
rt-smart的代码已经支持了GIC，本节视频把重点放在**移植**上，
**只需要指定GIC的物理地址**。



### 2. 指定GIC物理地址

修改：`rt-smart\kernel\bsp\stm32mp157\drivers\realview.h`

```c
#define REALVIEW_GIC_CPU_BASE       0xA0022000  /* Generic interrupt controller CPU interface */
#define REALVIEW_GIC_DIST_BASE      0xA0021000  /* Generic interrupt controller distributor */

```

