## rt-smart体验_STM32MP157

请参考GIT仓库中一下文件：

```c
doc_and_source_for_mcu_mpu\STM32MP157\
    基于STM32MP157的rt-smart开发手册.docx
```



支持rt-smart的开发板：

* 100ASK_STM32MP157 pro(全功能版)

![image-20201222173220035](pic/01_教程说明_资料下载_体验/018_100ask_stm32mo157_pro.png)

