## rt-smart驱动程序模型

### 1. APP和内核隔离

rt-smart中，APP无法直接访问硬件寄存器，必须通过驱动程序来操作硬件。
而APP和内核是隔离的，APP无法直接调用内核中驱动程序的函数。
APP使用驱动的流程如下图所示：
![](pic/06_串口移植/001_rtt_app_driver.png)

左边的rt_device_open等函数，它最终都是调用一条汇编指令`svc 0`，这会触发异常，进入内核态：

![image-20201217171615228](pic/06_串口移植/002_rtt_syscall_example.png)

在内核的异常处理函数里，会根据触发异常的原因，调用各类函数，比如：

```c
APP调用rt_device_init，最终导致驱动程序的init函数被调用；
APP调用rt_device_open，最终导致驱动程序的open函数被调用；
……
```



### 2. 驱动程序的核心

编写驱动程序的核心，就是编写那些init、open函数，然后告诉内核。
为了便于管理，这些函数使用结构体`rt_device_ops`来描述，这就是驱动程序的核心。

rt_device_ops结构体定义如下：

```c
struct rt_device_ops
{
    /* common device interface */
    rt_err_t  (*init)   (rt_device_t dev);
    rt_err_t  (*open)   (rt_device_t dev, rt_uint16_t oflag);
    rt_err_t  (*close)  (rt_device_t dev);
    rt_size_t (*read)   (rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size);
    rt_size_t (*write)  (rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size);
    rt_err_t  (*control)(rt_device_t dev, int cmd, void *args);
};
```



### 3. 注册驱动程序

示例代码如下：

```c
const static struct rt_device_ops clcd_ops =
{
    rt_lcd_init,
    RT_NULL,
    RT_NULL,
    RT_NULL,
    RT_NULL,
    rt_lcd_control
};

struct rt_device lcd_device;

lcd_device.ops = &clcd_ops;

rt_device_register(&lcd_device, "lcd", RT_DEVICE_FLAG_RDWR);
```



所以，编写驱动程序的套路为：

* 分配一个rt_device结构体，它表示一个**设备**

* 构造一个rt_device_ops结构体，它表示**设备的操作**，填充里面的函数

* rt_device结构体.ops  = rt_device_ops结构体

* 注册rt_deivce结构体：

  ```c
  rt_err_t rt_device_register(rt_device_t dev,
                              const char *name,
                              rt_uint16_t flags);
  ```

从注册过程可以知道，以后APP就是根据name来找到驱动程序。



### 4. APP使用驱动程序

![](pic/06_串口移植/001_rtt_app_driver.png)

示例代码为：

```c
struct rt_device *device;
device = rt_device_find("lcd");
rt_device_open(device, RT_DEVICE_OFLAG_RDWR);
```

