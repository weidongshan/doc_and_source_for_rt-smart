## scons介绍

参考文档：

```
SCons 构建工具
https://www.rt-thread.org/document/site/programming-manual/scons/scons/
```


### 1. 配置文件如何起作用

执行menuconfig命令来配置rt-smart时，配置结果保存在.config文件里，并且会被转换为头文件rtconfig.h。
rtconfig.h文件怎么起到配置作用呢？它在两方面起作用：

* 代码里使用：

  * rtconfig.h文件里含有每一个配置项，比如：

    ```c
#define RT_TICK_PER_SECOND 100
    #define RT_USING_OVERFLOW_CHECK
    #define RT_USING_HOOK
    #define RT_USING_IDLE_HOOK
    ```
    
    在c文件里就可以包含rtconfig.h文件，获得这些配置信息

  * 示例：

    ![image-20201223092748596](pic/03_Kconfig和Makefile/008_rtconfig.h.png)

*  脚本里使用：

  rt-smart使用scons来编译，会用到很多名为SConscript的脚本文件。
  在SConscript中，使用内置函数`GetDepend(macro)`从 rtconfig.h 文件读取配置信息：如果 rtconfig.h 打开了某个宏，则这个函数返回真，否则返回假。

  示例：

  ![image-20201223093543462](pic/03_Kconfig和Makefile/009_SConscript.png)



### 2. scons脚本

SCons 使用 SConscript 和 SConstruct 文件来组织源码结构，通常来说一个项目只有一 SConstruct，但是会有多个 SConscript。一般情况下，每个存放有源代码的子目录下都会放置一个 SConscript。

为了使 RT-Thread 更好的支持多种编译器，以及方便的调整编译参数，RT-Thread 为每个 BSP 单独创建了一个名为 rtconfig.py 的文件。因此每一个 RT-Thread BSP 目录下都会存在下面三个文件：rtconfig.py、SConstruct 和 SConscript，它们控制 BSP 的编译。一个 BSP 中只有一个 SConstruct 文件，但是却会有多个 SConscript 文件，可以说 SConscript 文件是组织源码的主力军。



#### 2.1 常用scons命令

* 编译：

  ```shell
  scons
  scons -j N   // 在多核计算机上可以使用此命令加快编译速度。
               // 一般来说一颗 cpu 核心可以支持 2 个线程。
               // 双核机器上使用 scons -j4 命令即可。
  ```

  

* 清除

  ```shell
  scons -c
  ```

#### 2.2 怎么编译一个目录

##### 2.2.1 SConscript文件

每个目录下都有一个SConscript 文件，里面使用DefineGroup来指定要编译的文件、编译参数等待。
以`rt-smart\kernel\bsp\qemu-vexpress-a9\drivers\SConscript`为例：
![image-20201223100652141](pic/03_Kconfig和Makefile/010_compiler_dir.png)



##### 2.2.2 DefineGroup用法

DefineGroup的格式为：**DefineGroup(name， src， depend，**parameters)**

这是 RT-Thread 基于 SCons 扩展的一个方法（函数）。DefineGroup 用于定义一个组件。组件可以是一个目录（下的文件或子目录），也是后续一些 IDE 工程文件中的一个 Group 或文件夹。

`DefineGroup()` 函数的参数描述：

| **参数**   | **描述**                                                     |
| ---------- | ------------------------------------------------------------ |
| name       | Group 的名字                                                 |
| src        | Group 中包含的文件，一般指的是 C/C++ 源文件。方便起见，也能够通过 Glob 函数采用通配符的方式列出 SConscript 文件所在目录中匹配的文件 |
| depend     | Group 编译时所依赖的选项（例如 FinSH 组件依赖于 RT_USING_FINSH 宏定义）。编译选项一般指 rtconfig.h 中定义的 RT_USING_xxx 宏。当在 rtconfig.h 配置文件中定义了相应宏时，那么这个 Group 才会被加入到编译环境中进行编译。如果依赖的宏并没在 rtconfig.h 中被定义，那么这个 Group 将不会被加入编译。相类似的，在使用 scons 生成为 IDE 工程文件时，如果依赖的宏未被定义，相应的 Group 也不会在工程文件中出现 |
| parameters | 配置其他参数，可取值见下表，实际使用时不需要配置所有参数     |

parameters 可加入的参数：

| **参数**   | **描述**                                         |
| ---------- | ------------------------------------------------ |
| CCFLAGS    | C 源文件编译参数                                 |
| CPPPATH    | 头文件路径                                       |
| CPPDEFINES | 链接时参数                                       |
| LIBRARY    | 包含此参数，则会将组件生成的目标文件打包成库文件 |



##### 2.2.3 rtconfig.py文件

在DefineGroup中没有指定更多编译参数是，使用rtconfig.py中定义的编译参数：

![image-20201223101223025](pic/03_Kconfig和Makefile/011_rtconfig.py.png)



#### 2.3 编译哪些目录

只要一个目录里含有SConscript文件，这个目录就会被处理。编译哪些文件，有改目录里的SConscript文件决定。
以`rt-smart\kernel\bsp\qemu-vexpress-a9`单板为例，这个目录里有一个`SConscript`文件：
![image-20201223101622349](pic/03_Kconfig和Makefile/012_process_dirs.png)



`rt-smart\kernel\bsp\qemu-vexpress-a9`目录下有`drivers`子目录，里面有SConscript文件，所以`drivers`子目录会被处理。
drviers子目录里的SConscript文件内容如下：
![image-20201223102011278](pic/03_Kconfig和Makefile/013_process_subdir.png)



#### 2.4 怎么链接

在rtconfig.py里指定了链接参数：

![image-20201223102115181](pic/03_Kconfig和Makefile/014_link.png)


