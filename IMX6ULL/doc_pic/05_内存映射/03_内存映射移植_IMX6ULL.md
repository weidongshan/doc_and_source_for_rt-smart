## 内存映射移植

本节修改好的代码放在GIT仓库里：
```
doc_and_source_for_rt-smart\IMX6ULL\source\02_录制视频时现场编写的源码\02_memmap_imx6ull\imx6ull
```

### 1. 根据芯片手册确定PV_OFFSET

* 先确定内存的物理基地址：芯片手册`IMX6ULLRM.pdf`

![image-20201217114612321](pic/05_内存映射/23_imx6ull_mem_phyaddr.png)



* 内存的虚拟基地址：KERNEL_VADDR_START = 0xC0000000
* 所以：PV_OFFSET = 0x80000000 - 0xC0000000 = 0xC0000000

* 最后，在menuconfig里配置PV_OFFSET

### 2. 根据单板资源确定内存大小

修改`platform_mem_desc`数组中的vaddr_end，
`platform_mem_desc`数组在`rt-smart\kernel\bsp\imx6ull\drivers\board.c`，如下：
![image-20201217084613901](pic/05_内存映射/21_platform_mem_desc.png)

100ASK_IMX6ULL的DDR是512M，即0x20000000，上述代码中`0x0fffffff`改为`0x1fffffff`。