## Kconfig介绍

参考文档：

```
任一个Linux内核的Documentation\kbuild\kconfig-language.rst

https://www.rt-thread.org/document/site/programming-manual/kconfig/kconfig/
```

对于各类内核，只要支持menuconfig配置界面，都是使用Kconfig。
在配置界面中，可以选择、设置选项，这些设置会保存在.config文件里。
编译脚本会包含.config，根据里面的值决定编译哪些文件、怎么编译文件。
.config文件也会被转换为头文件，C程序可以从头文件中获得配置信息。

### 1.1 配置界面示例

![image-20201215105722795](pic/03_Kconfig和Makefile/001_Kconfig_menuconfig.png)

问题：

* 这个界面里，各个配置项来自哪里
* 这个界面里，这些配置项是怎么组织的
* 这个界面里，我们的选择、设置，结果保存在哪里
* 这些配置结果，怎么使用

### 1.2 配置结果的保存

#### 1.2.1 示例

在配置界面中操作的结果保存在.config文件中，示例如下：

```
#
# RT-Thread Kernel
#
CONFIG_RT_NAME_MAX=8
# CONFIG_RT_USING_ARCH_DATA_TYPE is not set
CONFIG_RT_USING_SMART=y
CONFIG_RT_USING_SMP=y
CONFIG_RT_CPUS_NR=2
CONFIG_RT_ALIGN_SIZE=4
# CONFIG_RT_THREAD_PRIORITY_8 is not set

```

编译脚本会包含.config文件，它会根据里面的变量比如`CONFIG_RT_USING_SMART`选择内核特性。

#### 1.2.2 配置项的前缀

在Kconfig文件中，假设配置项的名字是XXX，在.config文件中：

* 默认情况下，它对应的变量名为`CONFIG_XXX`
* 如果设置了环境变量`CONFIG_=ABC`，则对应的变量名为`ABC_XXX`

  

### 1.3 描述单个配置项config

#### 1.3.1 示例

在`menuconfig`界面，可以看到这个配置项：

![image-20201215110848308](pic/03_Kconfig和Makefile/002_Kconfig_item_example.png)

在配置界面，使用方向箭头游走到`Enable UART1`后，可以：

* 输入Y，选择配置项，在.config中对应`CONFIG_RT_USING_UART1=y`

* 输入N，不选择配置项，在.config中对应`# CONFIG_RT_USING_UART1 is not set`

  

上图中的配置项怎么实现的？
在Kconfig文件中，它对应下列代码：

![image-20201215113304675](pic/03_Kconfig和Makefile/003_config_item_example.png)


#### 1.3.2 语法
上面是一个精简的例子，完整的例子可以从Linux中获得，如下：

```shell
config SGI_SNSC
        bool "SGI Altix system controller communication support"
        depends on (IA64_SGI_SN2 || IA64_GENERIC)
        default y
        help
          If you have an SGI Altix and you want to enable system
          controller communication from user space (you want this!),
          say Y.  Otherwise, say N.
```
解释如下：

* config
  表示`config option`，这是Kconfig的基本entry；其他entry是用来管理config的。
  config 表示一个配置选项的开始，紧跟着的 SGI_SNSC 是配置选项的名称。
  config 下面几行定义了该配置选项的属性。
  属性可以是该配置选项的：类型、输入提示、依赖关系、默认值、帮助信息。
  - bool 表示配置选项的类型，每个 config 菜单项都要有类型定义，变量有5种类型
    - bool 布尔类型
    - tristate 三态类型
    - string 字符串
    - hex 十六进制
    - int 整型
  - "SGI Altix system controller communication support"：提示信息
  - depends on：表示依赖关系，只有(IA64_SGI_SN2 || IA64_GENERIC)被选中，才可以选择SGI_SNSC
  - select XXX：表示反向依赖关系，即当前配置选项被选中后，`XXX`选项就会被选中。
  - default 表示配置选项的默认值，bool 类型的默认值可以是 y/n。
  - help 帮助信息，在`menuconfig`界面输入H键时，就会提示帮助信息。

### 1.4 实现菜单menu/endmenu

#### 1.4.1 示例

示例代码：`rt-smart/kernel/src/Kconfig`，代码如下：

```shell
menu "Inter-Thread communication"

config RT_USING_SEMAPHORE
    bool "Enable semaphore"
    default y

config RT_USING_MUTEX
    bool "Enable mutex"
    default y

config RT_USING_EVENT
    bool "Enable event flag"
    default y

config RT_USING_MAILBOX
    bool "Enable mailbox"
    default y

config RT_USING_MESSAGEQUEUE
    bool "Enable message queue"
    default y

config RT_USING_SIGNALS
    bool "Enable signals"
    select RT_USING_MEMPOOL
    default n
    help
        A signal is an asynchronous notification sent to a specific thread
        in order to notify it of an event that occurred.
endmenu
```

界面如下：

![image-20201123181351893](pic/03_Kconfig和Makefile/004_menuconfig_menu_example.png)

#### 1.4.2 语法

解释如下：

* menu "xxx"表示一个菜单，菜单名是"xxx"

* menu和endmenu之间的entry都是"xxx"菜单的选项

* 在上面的例子中子菜单有6个选项：

  * "Enable semaphore"
  * "Enable mutex"
  * "Enable event flag"
  * "Enable mailbox"
  * "Enable message queue"
  * "Enable signals"

* 第二个菜单项"Enable mutex"，因为被其他配置项选中了，无法去修改它，它的表现形式不一样

  

### 1.5 实现单选choice/endchoice

#### 1.5.1 示例

示例代码：`rt-smart/kernel/src/Kconfig`，代码如下：

```shell
config RT_ALIGN_SIZE
    int "Alignment size for CPU architecture data access"
    default 4
    help
        Alignment size for CPU architecture data access

    choice
        prompt "The maximal level value of priority of thread"
        default RT_THREAD_PRIORITY_32

        config RT_THREAD_PRIORITY_8
            bool "8"

        config RT_THREAD_PRIORITY_32
            bool "32"

        config RT_THREAD_PRIORITY_256
            bool "256"
    endchoice
```

界面如下：

![image-20201123180708902](pic/03_Kconfig和Makefile/005_menuconfig_choice_example.png)



在上述界面中，对于`RT_ALIGN_SIZE`，有3个选择：

* RT_THREAD_PRIORITY_8
* RT_THREAD_PRIORITY_32
* RT_THREAD_PRIORITY_256

#### 1.5.2 语法

解释如下：

* choice表示"选择"
* choice和endchoice之间的entry是可以选择的项目
  * 它们之间，只能有一个被设置为"y"：表示编进内核
  * 它们之间，可以设置多个为"m"：表示编译为模块
  * 比如一个硬件有多个驱动程序
    * 同一时间只能有一个驱动能编进内核
    * 但是多个驱动都可以单独编译为模块



### 1.6 menuconfig

`menuconfig XXX`和`config XXX`类似，
唯一不同的是该选项除了能设置y/m/n外，还可以实现菜单效果(**能回车进入该项内部**)。

#### 1.6.1 示例

示例代码：`rt-smart\kernel\components\drivers\Kconfig`

```shell
        menuconfig RT_WLAN_DEBUG
            bool "Enable WLAN Debugging Options"
            default n

        if RT_WLAN_DEBUG
            config RT_WLAN_CMD_DEBUG
                bool "Enable Debugging of wlan_cmd.c"
                default n

            config RT_WLAN_MGNT_DEBUG
                bool "Enable Debugging of wlan_mgnt.c"
                default n

            config RT_WLAN_DEV_DEBUG
                bool "Enable Debugging of wlan_dev.c"
                default n

            config RT_WLAN_PROT_DEBUG
                bool "Enable Debugging of wlan_prot.c"
                default n

            config RT_WLAN_CFG_DEBUG
                bool "Enable Debugging of wlan_cfg.c"
                default n

            config RT_WLAN_LWIP_DEBUG
                bool "Enable Debugging of wlan_lwip.c"
                default n
        endif
```

界面如下：

![image-20201124075712019](pic/03_Kconfig和Makefile/006_menuconfig_menuconfig_example.png)



#### 1.6.2 语法

menuconfig常用格式有2种：

```shell
  menuconfig M
  if M
      config C1
      config C2
  endif
```

或：

```shell
  menuconfig M
  config C1
      depends on M
  config C2
      depends on M
```

第1项`menuconfig M`跟`config M`语法是一样的，
不同之处在于`menuocnfig M`后面可以跟着好几个依赖于M的`config C1`、`config C2`等子配置项。

### 1.7 if/endif

#### 1.7.1 语法

在上面的menuconfig中就有`if/endif`的使用，它的语法如下：

```shell
"if" <expr>
<if block>
"endif"
```

#### 1.7.2 示例

示例如下，只有定义了的RT_WLAN_DEBUG项，`RT_WLAN_CMD_DEBUG`等才会显示出来：

```shell
if RT_WLAN_DEBUG
            config RT_WLAN_CMD_DEBUG
                bool "Enable Debugging of wlan_cmd.c"
                default n

            config RT_WLAN_MGNT_DEBUG
                bool "Enable Debugging of wlan_mgnt.c"
                default n

            config RT_WLAN_DEV_DEBUG
                bool "Enable Debugging of wlan_dev.c"
                default n

            config RT_WLAN_PROT_DEBUG
                bool "Enable Debugging of wlan_prot.c"
                default n

            config RT_WLAN_CFG_DEBUG
                bool "Enable Debugging of wlan_cfg.c"
                default n

            config RT_WLAN_LWIP_DEBUG
                bool "Enable Debugging of wlan_lwip.c"
                default n
        endif
```

### 1.8 source

source 语句用于读取另一个文件中的 Kconfig 文件，
比如`rt-smart\kernel\bsp\qemu-vexpress-a9\Kconfig`中就包含了其他Kconfig：

```shell
source "$BSP_DIR/drivers/Kconfig"
```

### 1.9 comment 

comment 语句出现在界面的第一行，用于定义一些提示信息，如：

```shell
config ARCH_ARM
   bool

source "arch/arm/Kconfig"

comment "Extra Configurations"

config ARCH_FPU_DISABLE
    bool "Disable Floating Pointer Unit"
    default n
    help
      This option will bypass floating procedure in system.
```

界面如下：
![image-20201124081426328](pic/03_Kconfig和Makefile/007_menuconfig_comment_example.png)

### 1.10 测试代码

```shell
menu "test menu"
config TEST_A
    bool "TEST A"
    default y
    
config TEST_B
    bool "TEST B"
    default y

endmenu

choice
        prompt "test choise"
        default TEST_C

config TEST_C
    bool "TEST C"
    default y
    
config TEST_D
    bool "TEST D"
    default y

endchoice        

menuconfig TEST_MENUCONFIG
    bool "test menuconfig"
    default n
    
if TEST_MENUCONFIG
config TEST_E
    bool "TEST E"
    default y

config TEST_F
    bool "TEST F"
    default y


endif
```

