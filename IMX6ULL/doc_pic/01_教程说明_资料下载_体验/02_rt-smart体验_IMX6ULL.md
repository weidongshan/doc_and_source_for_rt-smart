## rt-smart体验_IMX6ULL

请参考GIT仓库中一下文件：

```c
doc_and_source_for_mcu_mpu\IMX6ULL\
    IMX6ULL裸机开发完全手册.docx
```



支持rt-smart的开发板：

* 100ASK_IMX6ULL pro(全功能版)

  ![image-20201222171250656](pic/01_教程说明_资料下载_体验/016_100ask_imx6ll_pro.png)

* 100ASK_IMX6ULL mini emmc：

  ![image-20201222171430317](pic/01_教程说明_资料下载_体验/017_100ask_imx6ll_mini_emmc.png)