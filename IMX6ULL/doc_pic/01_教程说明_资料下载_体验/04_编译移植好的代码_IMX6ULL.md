## 编译移植好的代码_IMX6ULL

代码在GIT仓库中，如下目录：

```shell
doc_and_source_for_rt-smart\IMX6ULL\source\
	02_录制视频时现场编写的源码\
		04_timer_bringup_ok_imx6ull\
			imx6ull
```



### 1. 复制代码

把上述imx6ull目录复制到：`D:\rtthread\rt-smart-20201125\rt-smart\kernel\bsp`



### 2. 编译APP

```shell
# 进入 userapps 目录进行编译
cd d:\rtthread\rt-smart-20201125\rt-smart\userapps\
scons
```

编译成功后，`userapps/apps` 下的应用程序会被编译成一个个的 elf 可执行文件，并放置于 `userapps/root/bin` 目录下。
可以把它们转换成 romfs，以C 语言数组的方式给 rt-smart 内核使用，这样可以不依赖于其他文件系统即可执行官APP。
生成 romfs、C 语言数组可以用如下的命令行：

```bash
cd d:\rtthread\rt-smart-20201125\rt-smart\userapps\
python ../tools/mkromfs.py root ../kernel/bsp/imx6ull/applications/romfs.c
```



### 3. 编译内核
```shell
# 进入 userapps 目录进行编译
cd d:\rtthread\rt-smart-20201125\rt-smart\kernel\bsp\imx6ull
scons
```


### 4. 运行

