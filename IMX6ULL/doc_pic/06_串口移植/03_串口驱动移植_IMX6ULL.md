## 串口驱动移植

参考资料：

* rt-smart：`rt-smart\kernel\bsp\qemu-vexpress-a9\drivers\serial.c`
* 裸机代码：`git clone https://e.coding.net/weidongshan/noos/doc_and_source_for_mcu_mpu.git`
* 参考视频：[百问网](http://www.100ask.net)单片机RTOS必备/硬件编程

本节视频修改的最终代码在GIT仓库如下目录里：

```c
doc_and_source_for_rt-smart\IMX6ULL\source\02_录制视频时现场编写的源码\03_uart_imx6ull\imx6ull
```



### 1. 指定硬件资源

![image-20201218102352102](pic/06_串口移植/006_uart_resource.png)

### 2. 实现操作函数

![image-20201218102549616](pic/06_串口移植/007_uart_ops.png)



### 3. 使用中断接收数据

如果使用查询方式读取串口数据，低效而费电。一般都使用中断方式读取串口数据。
内核已经提供了通用的中断处理函数，发生中断时，它会使用rt_uart_ops里的getc函数去读取数据。



### 4. 编写程序

配置内核禁止 RT_USING_UART1 、 RT_USING_SMP