## EMMC启动

本节视频修改的最终代码在GIT仓库如下目录里：

```c
doc_and_source_for_rt-smart\IMX6ULL\source\02_录制视频时现场编写的源码\
    05_emmc_boot_ok_imx6ull\
    	imx6ull
```


### 1. 烧写到EMMC
#### 1.1 文件太大无法烧写到EMMC

需要裁剪romfs。

##### 1.1.1 编译、裁剪APP

```shell
# 进入 userapps 目录进行编译
cd d:\rtthread\rt-smart-20201125\rt-smart\userapps\
scons
```

编译成功后，`userapps/apps` 下的应用程序包含有调试信息，比较大，需要裁剪一下，执行下面的命令：

```shell
cd d:\rtthread\rt-smart-20201125\rt-smart\userapps
arm-linux-musleabi-strip.exe root\bin\hello.elf
arm-linux-musleabi-strip.exe root\bin\ping.elf
arm-linux-musleabi-strip.exe root\bin\pong.elf
arm-linux-musleabi-strip.exe root\bin\vi.elf
arm-linux-musleabi-strip.exe root\bin\webclient.elf
arm-linux-musleabi-strip.exe root\bin\webserver.elf
```



最后重新生成 romfs、C 语言数组，执行如下的命令行：

```bash
cd d:\rtthread\rt-smart-20201125\rt-smart\userapps\
python ../tools/mkromfs.py root ../kernel/bsp/imx6ull/applications/romfs.c
```



##### 1.1.2 编译内核

```shell
# 进入 userapps 目录进行编译
cd d:\rtthread\rt-smart-20201125\rt-smart\kernel\bsp\imx6ull
scons
```

可以得到更小的rtthread.imx文件。




#### 1.2. EMMC启动无输出



### 2. 修改串口驱动程序

![image-20201224151705654](pic/08_改进程序/001_uart_improve.png)

### 3. 总结

USB启动时，boot rom设置了串口；
EMMC启动时，boot rom没有设置串口，需要我们去初始化串口。

