## GenericTimer主要代码编程

参考资料：

* `ARM ArchitectureReference Manual ARMv7-A and ARMv7-R edition.pdf`
  
  * 《B8: The Generic Timer》
  * 《D5: System Level Implementation of the Generic Timer》
  
* STM32MP157芯片手册`DM00327659.pdf`

  * 《46 System timer generator (STGEN)》
  
* [Linux时间子系统之（十七）：ARM generic timer驱动代码分析](http://www.wowotech.net/timer_subsystem/armgeneraltimer.html)

  

### 1. RTOS中定时器要做的事情
* 初始化、使能定时器
* 设置中断函数
* 中断函数要做的事情：
  * 调用内核提供的tick函数
  * 设置下一次中断
  * 清除中断

### 2. GenericTimer主要代码

根据下图设置3部件：时钟源、SystemCounter、Processor Timer。

```mermaid
graph LR
clk_source[设置时钟源]
SystemCounter[设置/启动SystemCounter]
timer0["设置Processor0的Timer: 设置比较值、使能中断、使能Timer"]
timer1["设置Processor1的Timer: 设置比较值、使能中断、使能Timer"]
clk_source --> SystemCounter --> timer0
SystemCounter --> timer1
```


#### 2.1 时钟源的设置

时钟源跟芯片密切相关，用来给SystemCounter提供时钟。
一般片内ROM已经设置好了，这部分比较复杂，需要阅读芯片手册了解它的时钟体系。

#### 2.2 SystemCounter的设置

主要是：

* 使能SystemCounter
* 为了方便程序获知频率，把时钟源的频率写入Processor Timer寄存器

#### 2.3 Processor Timer的设置

主要是：

* 设置下一次中断的时间
* 使能中断
* 启动Processor Timer

参考代码：Linux源码 `arch/arm/include/asm/arch_timer.h`